/**
 * Module dependencies.
 */
/*global require, console, process, __dirname*/

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var http = require('http');
var path = require('path');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
	app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/users', user.list);




////////////////////////////////////////////////////////
app.get('/poker', function (req, res) {
	//jl.addlog2('startactioncb', 'feed_get');
	//statstracker.insertImpression('feed_get');

	res.render('games/poker.ejs', {
		title: "Poker - 5 Card Hand",
		bodyjson: 'empty' //none yet when hitting the page on a get request // data
	});
});
////////////////////////////////////////////////////////








http.createServer(app).listen(app.get('port'), function () {
	console.log('Express server listening on port ' + app.get('port'));
});