/**
determine which poker hand wins

10. Royal Flush
9. Straight Flush
8. Four Of A Kind
7. Full House
6. Flush
5. Straight
4. Three Of A Kind
3. Two Pairs
2. One Pair
1. High Card
0. invalid


*/
/*globals document, setTimeout, navigator, event*/



var hands = ["4 of a Kind", "Straight Flush", "Straight", "Flush", "High Card",
       "1 Pair", "2 Pair", "Royal Flush", "3 of a Kind", "Full House", "-Invalid-"];

var handRanks = [8, 9, 5, 6, 1, 2, 3, 10, 4, 7, 0]; //???

var finalIndex_hand1 = 10; //-invalid-
var finalIndex_hand2 = 10; //-invalid-

var highCard_hand1 = 0;
var highCard_hand2 = 0;

//lowest to highest. based off the 'hands' array
var winnerRank = {
	"4 of a Kind": 8,
	"Straight Flush": 9,
	"Straight": 5,
	"Flush": 6,
	"High Card": 1,
	"1 Pair": 2,
	"2 Pair": 3,
	"Royal Flush": 10,
	"3 of a Kind": 4,
	"Full House": 7,
	"-Invalid-": 0
};




/**
for inputCardSymbolsOnly()
Setup a collection of substitutions
readonly
*/
var keyMap = {
	"a": "A",
	"k": "K",
	"q": "Q",
	"j": "J",
	"A": "A",
	"K": "K",
	"Q": "Q",
	"J": "J",
	"s": "♠",
	"c": "♣",
	"h": "♥",
	"d": "♦",
	"S": "♠",
	"C": "♣",
	"H": "♥",
	"D": "♦",
	"@": "A",
	"&": "K",
	"$": "Q",
	")": "J",
	"?": "♠",
	"!": "♣",
	"'": "♥",
	'"': "♦"
};



/////////////////////////////////////////////////////////////
//functions







/**
eh - each keypress

*/
function inputCardSymbolsOnly(e) {
	console.log('inputCardSymbolsOnly');
	console.log('e', e);

	if (!e) {
		return false;
	}



	var chrTyped,
		chrCode = 0,
		evt = e ? e : event;

	if (evt.charCode !== undefined) {
		chrCode = evt.charCode;
	} else if (evt.which !== undefined) {
		chrCode = evt.which;
	} else if (evt.keyCode !== undefined) {
		chrCode = evt.keyCode;
	}

	if (chrCode === 0) {
		chrTyped = 'SPECIAL KEY';
	} else {
		chrTyped = String.fromCharCode(chrCode);
	}

	//[test only:] display chrTyped on the status bar 
	//document.getElementById("output").innerHTML += chrCode;


	if (chrTyped in keyMap) {
		//evt.returnValue = false; //Super important do not move!!!. note: did not work in ie or firefox. double character was created
		evt.preventDefault();

		if (document.selection) {
			//IE  
			var range = document.selection.createRange();
			range.text = keyMap[chrTyped];
			if (evt.preventDefault !== undefined) {
				evt.preventDefault();
			}
			// Chrome + FF
		} else if (e.target.selectionStart || e.target.selectionStart == '0') {
			var start = evt.target.selectionStart;
			var end = evt.target.selectionEnd;

			evt.target.value = evt.target.value.substring(0, start) + keyMap[chrTyped] +
				evt.target.value.substring(end, evt.target.value.length);
			evt.target.selectionStart = start + 1;
			evt.target.selectionEnd = start + 1;
		} else {
			evt.target.value += keyMap[chrTyped];
		}

		return false;
	}

	if (chrTyped.match(/\d|\s|SPECIAL/)) {
		evt.returnValue = true;
		return true;
	}
	if (evt.altKey || evt.ctrlKey || chrCode < 28) {
		return true;
	}

	//Any other input? Prevent the default response:
	if (evt.preventDefault) {
		evt.preventDefault();
	}
	evt.returnValue = false;

	//evt.charCode = '';
	return false;
}









/**
eh - keyup and input

*/
function doRank(e) {
	console.log('doRank');
	console.log(e); //

	//trick to do async call
	setTimeout(function () {
		rankHand(document.getElementById("cardInput").value, 'hand1');
		rankHand(document.getElementById("cardInput2").value, 'hand2');
	}, 0);
}








/**
caller: dorank via asynchronous call

*/
function rankHand(str, hand1or2) {
	console.log('rankHand');
	console.log('str', str); // the digit that was typed in, or one of the 4 suits typed in. builds up over many key presses... 8♣10♥7♣9 

	var isHand1 = (hand1or2 == 'hand1'); //is this hand1 or hand2 ?
	var extraDigit20 = (hand1or2 == 'hand1' ? 0 : 20);

	var index = 10,
		//winCardIndexes,
		i,
		e;
	var wci;



	//invoke
	showParsedCards(str.match(/(1[0-4]|[2-9]|[J|Q|K|A])/g), str.match(/♠|♣|♥|♦/g), hand1or2); //pass in number, then suit
	console.log('after', 'showParsedCards');
	//console.log('str, 2nd time', str);

	if (str.match(/((?:\s*)(10|[2-9]|[J|Q|K|A])[♠|♣|♥|♦](?:\s*)){5,7}/g) !== null) {
		console.log('true', 'str had a match'); //only true if 5 cards, so enough to produce a ranking


		var cardStr = str
			.replace(/A/g, "14")
			.replace(/K/g, "13").replace(/Q/g, "12")
			.replace(/J/g, "11").replace(/♠|♣|♥|♦/g, ",");

		console.log('cardStr', cardStr);

		var cards = cardStr.replace(/\s/g, '').slice(0, -1).split(",");

		console.log('cards', cards); // ["7", "10", "11", "8", "9"]




		var largestNumber = Math.max.apply(Math, cards);
		console.log('largestNumber', largestNumber);

		if (isHand1) {
			highCard_hand1 = largestNumber;
		} else {
			highCard_hand2 = largestNumber;
		}




		var suits = str.match(/♠|♣|♥|♦/g);

		if (cards !== null && suits !== null) {

			if (cards.length == suits.length) {
				var o = {},
					keyCount = 0,
					j;

				for (i = 0; i < cards.length; i++) {
					e = cards[i] + suits[i];
					o[e] = 1;
				}

				for (j in o) {
					if (o.hasOwnProperty(j)) {
						keyCount++;
					}
				}


				console.log('cards.length', cards.length);


				if (cards.length >= 5) {
					console.log('true, so we have enough cards to diplay the hand ranking');

					if (cards.length == suits.length && cards.length == keyCount) {

						for (i = 0; i < cards.length; i++) {
							cards[i] -= 0;
						}

						for (i = 0; i < suits.length; i++) {
							suits[i] = Math.pow(2, (suits[i].charCodeAt(0) % 9824));
						}

						var c = getCombinations(5, cards.length);
						var maxRank = 0,
							winIndex = 10;

						for (i = 0; i < c.length; i++) {
							var cs = [cards[c[i][0]], cards[c[i][1]], cards[c[i][2]], cards[c[i][3]], cards[c[i][4]]];
							var ss = [suits[c[i][0]], suits[c[i][1]], suits[c[i][2]], suits[c[i][3]], suits[c[i][4]]];

							index = calcIndex(cs, ss);

							console.log('index', index);
							var handranksindex = handRanks[index];
							console.log('handranksindex', handranksindex);

							if (handRanks[index] > maxRank) {
								maxRank = handRanks[index];
								winIndex = index;

								console.log('maxRank', maxRank);
								console.log('winIndex', winIndex);

								wci = c[i].slice();
							} else if (handRanks[index] == maxRank) {
								//If by chance we have a tie, find the best one
								var score1 = getPokerScore(cs);
								var score2 = getPokerScore([cards[wci[0]], cards[wci[1]], cards[wci[2]], cards[wci[3]], cards[wci[4]]]);
								if (score1 > score2) {
									wci = c[i].slice();
								}
							}
						} //endfor

						index = winIndex;
					}

					//cards = [];
				} //endif, 5 cards

				//Show the best cards if cs.length is less than 7 cards.
				var card;

				if (cards.length <= 7) { //from 7 to 5

					for (i = 0; i < 7; i++) {
						card = document.getElementById("card" + (i + 1 + extraDigit20));

						if (wci.indexOf(i) == -1) {

							//Not in the solution
							if (card.parentNode.tagName == "STRONG") {
								card.parentNode.parentNode.innerHTML = card.outerHTML;
							}
						} else {

							//Is in the solution
							if (card.parentNode.tagName == "LI") {
								card.outerHTML = "<strong>" + card.outerHTML + "</strong>";
							}
						}
					}
				} //endif was 7 or less
			}
		}
	} else {
		console.log('else', 'match was null');
	}
	//endif of match not being null





	if (isHand1) {
		document.getElementById("output").innerHTML = '<h3><b>Hand: </b><span style="background-color:black;padding:2px;">' + hands[index] + '</span></h3>';
		finalIndex_hand1 = hands[index];
		//if(highCard_hand1

	} else {
		document.getElementById("output2").innerHTML = '<h3><b>Hand: </b><span style="background-color:black;padding:2px;">' + hands[index] + '</span></h3>';
		finalIndex_hand2 = hands[index];
	}



	console.log('finalIndex_hand1', finalIndex_hand1); //Straight 
	console.log('finalIndex_hand2', finalIndex_hand2); //Straight Flush

	var bothDone = finalIndex_hand1 !== "-Invalid-" && finalIndex_hand2 !== "-Invalid-";



	if (bothDone) {
		var rank_hand1 = winnerRank[finalIndex_hand1];
		var rank_hand2 = winnerRank[finalIndex_hand2];

		console.log('rank_hand1', rank_hand1); //5
		console.log('rank_hand2', rank_hand2); //9



		console.log('highCard_hand1 ', highCard_hand1); //6
		console.log('highCard_hand2 ', highCard_hand2); //8

		var winner = "Hand One";

		if (rank_hand2 > rank_hand1) {
			winner = "Hand Two";
		}


		var tie = (rank_hand1 === rank_hand2);

		if (tie) {
			console.log('true', 'was a tie');

			if (highCard_hand2 > highCard_hand1) {
				winner = "Hand Two";
			}
			if (highCard_hand2 == highCard_hand1) {
				winner = "It's a Draw!";
			}

		}

		document.getElementById("winner").innerHTML = '<h3 style="background-color:red;	padding: 2px;	color: white;">The Winner is: <b>' + winner + '</b></h3>';

	} //endif both done







} //eom




/**
caller: rankHand

*/
function showParsedCards(cs, ss, hand1or2) {
	console.log('showParsedCards');
	console.log('cs', cs); //["4"] 
	console.log('ss', ss); //["♦"] 

	var extraDigit20 = (hand1or2 == 'hand1' ? 0 : 20);
	//console.log('extraDigit20', extraDigit20);


	var card,
		i;
	var suitMap = {
		"♠": "spades",
		"♣": "clubs",
		"♥": "hearts",
		"♦": "diams"
	};

	if (cs !== null && ss !== null) {
		if (cs.length == ss.length) {

			for (i = 0; i < 7; i++) {
				console.log('i9', i);


				card = document.getElementById("card" + (i + 1 + extraDigit20));

				console.log('card', card);

				if (i < 5) {
					if (i < cs.length && cs.length !== 0) {
						card.className = "card rank-" + cs[i].toLowerCase() + " " + suitMap[ss[i]];
						card.getElementsByTagName("span")[0].innerHTML = cs[i];
						card.getElementsByTagName("span")[1].innerHTML = ss[i];

						console.log('card.className', card.className);

					} else {
						card.className = "card back";
						card.getElementsByTagName("span")[0].innerHTML = "";
						card.getElementsByTagName("span")[1].innerHTML = "";
						if (card.parentNode.tagName == "STRONG") {
							card.parentNode.parentNode.innerHTML = card.outerHTML;
						}
					}
				} else {
					if (i < cs.length && cs.length !== 0) {
						card.className = "card rank-" + cs[i].toLowerCase() + " " + suitMap[ss[i]];
						card.getElementsByTagName("span")[0].innerHTML = cs[i];
						card.getElementsByTagName("span")[1].innerHTML = ss[i];
					} else {
						card.className = "blank";
						card.getElementsByTagName("span")[0].innerHTML = "";
						card.getElementsByTagName("span")[1].innerHTML = "";
						if (card.parentNode.tagName == "STRONG") {
							card.parentNode.parentNode.innerHTML = card.outerHTML;
						}
					}
				}
				if (cs.length < 5) {
					if (card.parentNode.tagName == "STRONG") {
						card.parentNode.parentNode.innerHTML = card.outerHTML;
					}
				}
			}
		}
	} else {

		//Reset the cards
		for (i = 0; i < 7; i++) {
			console.log('i9b', i);


			card = document.getElementById("card" + (i + 1 + extraDigit20));

			if (i > 4 && i < 7) {
				card.className = "blank";
				card.getElementsByTagName("span")[0].innerHTML = "";
				card.getElementsByTagName("span")[1].innerHTML = "";
			} else {
				card.className = "card back";
				card.getElementsByTagName("span")[0].innerHTML = "";
				card.getElementsByTagName("span")[1].innerHTML = "";
			}
			if (card.parentNode.tagName == "STRONG") {
				card.parentNode.parentNode.innerHTML = card.outerHTML;
			}
		}
	}

	if (cs === null) {
		document.getElementById("wrapper").style.width = "425px";
	}
	if (cs !== null && ss !== null && cs.length <= 7 && cs.length == ss.length) {
		document.getElementById("wrapper").style.width = Math.max(85 * cs.length, 425) + "px";
	}
}






/**
caller rankHand

*/
function getPokerScore(cs) {
	console.log('called getpokerscore ' + cs);

	var a = cs.slice(),
		d = {}, i;
	for (i = 0; i < 5; i++) {
		d[a[i]] = (d[a[i]] >= 1) ? d[a[i]] + 1 : 1;
	}
	a.sort(function (a, b) {
		return (d[a] < d[b]) ? +1 : (d[a] > d[b]) ? -1 : (b - a);
	});
	return a[0] << 16 | a[1] << 12 | a[2] << 8 | a[3] << 4 | a[4];
}





/**
caller: rankHand

*/
function calcIndex(cs, ss) {
	console.log('calcIndex');

	var v, i, o, s;

	for (i = -1, v = o = 0; i < 5; i++, o = Math.pow(2, cs[i] * 4)) {
		v += o * ((v / o & 15) + 1);
	}

	if ((v %= 15) != 5) {
		return v - 1;
	} else {
		s = 1 << cs[0] | 1 << cs[1] | 1 << cs[2] | 1 << cs[3] | 1 << cs[4];
	}

	v -= ((s / (s & -s) == 31) || (s == 0x403c) ? 3 : 1);
	return v - (ss[0] == (ss[0] | ss[1] | ss[2] | ss[3] | ss[4])) * ((s == 0x7c00) ? -5 : 1);
}






/**
caller: rankHand

*/
function getCombinations(k, n) {
	console.log('getcombinations');
	console.log('k', k);
	console.log('n', n);

	var result = [],
		comb = [];



	/**
	inner function
	*/
	function next_comb(comb, k, n, i) {
		if (comb.length === 0) {
			for (i = 0; i < k; ++i) {
				comb[i] = i;
			}
			return true;
		}
		i = k - 1;
		++comb[i];
		while ((i > 0) && (comb[i] >= n - k + 1 + i)) {
			--i;
			++comb[i];
		}
		if (comb[0] > n - k) {
			return false;
		} // No more combinations can be generated
		for (i = i + 1; i < k; ++i) {
			comb[i] = comb[i - 1] + 1;
		}
		return true;
	}
	while (next_comb(comb, k, n)) {
		result.push(comb.slice());
	}
	return result;
}









/////////////////////////////////////////////////
/**
caller: globally call
- called 3 times onload

*/
function addEventHandler(elem, eventType, handler) {
	console.log('addEventHandler');
	//console.log('elem', elem); //input
	//console.log('eventType', eventType); //keypress, keyup, input
	//console.log('handler', handler); //inputCardSymbolsOnly, doRank, doRank


	if (!elem) {
		console.error("elem5 was null");
		return;
	}

	if (elem.addEventListener) {
		elem.addEventListener(eventType, handler, true);
	} else if (elem.attachEvent) {
		elem.attachEvent('on' + eventType, handler);
	} else {
		return 0;
	}
	return 1;
}









//////////////////////////////////////////////////////////////////////////////////////////////////
//global


//ipad shim
var isiPad = navigator.userAgent.match(/iPad/i) !== null;
if (isiPad) {
	document.getElementById("cardInput").type = "tel";
	document.getElementById("cardInput2").type = "tel";

	document.getElementById("output").innerHTML = "<small>Use ),$,&,@ to key in J,Q,K,A.<br/>" + "Use ?,!,',&quot; to key in suits.</small>";
	document.getElementById("output2").innerHTML = "<small>Use ),$,&,@ to key in J,Q,K,A.<br/>" + "Use ?,!,',&quot; to key in suits.</small>";
}


//wire up event handlers for input box
addEventHandler(document.getElementById("cardInput"), 'keypress', inputCardSymbolsOnly);
addEventHandler(document.getElementById("cardInput"), 'keyup', doRank);
addEventHandler(document.getElementById("cardInput"), 'input', doRank);

addEventHandler(document.getElementById("cardInput2"), 'keypress', inputCardSymbolsOnly);
addEventHandler(document.getElementById("cardInput2"), 'keyup', doRank);
addEventHandler(document.getElementById("cardInput2"), 'input', doRank);




document.getElementById('cardInput').focus();