var config = module.exports;

config["My buster tests"] = {
    rootPath: "../",
    environment: "browser", // or "node"
    sources: [
        "public/**/*.js"
    ],
    tests: [
        "test/**/*-test.js"
    ]
}

// Add more configuration groups as needed


